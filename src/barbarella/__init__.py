# Barbarella. A Gemini Server.
#
# Copyright (C) 2021  Duck Hunt-Pr0
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import configparser
import argparse



parser = argparse.ArgumentParser(prog="barbarella",
                                 description='''barbarella  Copyright (C) 2021  Duck Hunt-Pr0.\n\nA gemini server. Blablabla bla bla blabla. Blablabla bla bla blabla. Blablabla bla bla blabla.\nBlablabla bla bla blabla.''', formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog='''ballejalle...''')


parser.add_argument('-v','--version', help='show the version number and exit', action='store_true', default=False)
parser.add_argument('-d','--daemon', help='run in background', action='store_true', default=False)
parser.add_argument('-c','--config-file', help='configuration file',required=True,type=open)
parser.add_argument('-l','--log-file', help='log file')
parser.add_argument('-L','--log-level', help='log levels', type=str, required=False ,default='INFO')


args = parser.parse_args()

# logging setup
logging.basicConfig(format='%(asctime)s\t%(levelname)s:\t%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.DEBUG)

