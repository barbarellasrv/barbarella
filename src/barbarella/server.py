# Barbarella. A Gemini Server.
#
# Copyright (C) 2021  Duck Hunt-Pr0
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from . import *
from logging import log
from gevent.server import StreamServer
from time import sleep

class BarbarellaServer:
    def __init__(self, arguments:argparse.ArgumentParser=args):

        log(logging.INFO, '''Starting Barbarella...''')

        self.arguments = args



    def run(self):
        """start the server loop"""

        with self.arguments.config_file as balle:
            log(logging.DEBUG, balle.name)

        try:
            while True:
                log(logging.INFO, '''Barbarella running!''')
                sleep(5)

        except KeyboardInterrupt:
            log(logging.INFO,"Quitting...")