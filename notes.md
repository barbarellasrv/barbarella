

* https://www.gevent.org/contents.html
* https://en.wikipedia.org/wiki/Gemini_(protocol)
* https://gemini.circumlunar.space/docs/specification.html

Command to create self-signed key/cert:
```
openssl req -newkey rsa:2048 -nodes -keyout localhost.key -nodes -x509 -out localhost.crt -subj "/CN=localhost"
```

testing stuff
```
######### simple gevent gemini serving

from gevent.server import StreamServer
from gevent import socket
from gevent.ssl import SSLContext, PROTOCOL_TLS
import time

def handle(socket, address ):
    print('new connection!')


    rfileobj = socket.makefile(mode='rb')
    line = rfileobj.readline()
    print(line)

    socket.send(b'20 text/gemini\r\n')
    socket.send(b"# Ballefaen!\n")

    socket.send(b'```\n')
    socket.send(b' ## Some preformatted text!\n')
    socket.send(b'```\n\n')

    socket.send(b'```\n')
    socket.send(b"Request was: " + line )
    socket.send(b'```\n')


    print(address)
    socket.close()



server =  StreamServer(('127.0.0.1', 1965), handle, keyfile='./localhost.key', certfile='./localhost.crt'  )  # creates a new server
server.serve_forever()  # start accepting new connections
server.close()
```
